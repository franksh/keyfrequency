
Uses X11 events to log keys.

One dependency:

$ pip install xlib

Start collecting data:

$ python keycollect.py

It doesn't daemonize. Every 200th key down event, it stores the
aggregate counts in "data.coll" in the current directory.

After a while you can do this for a pretty ASCII keyboard:

$ python ascii_keyboard.py

You have to modify that Python script if you're not using a pc105
int'l keyboard. It uses the file "keyboard.txt" as a sort of canvas,
putting in key names and counts wherever it finds a period (".").

$ python showkey.py

Simply prints the X11 keycode events as they're intercepted. Useful
for finding out what keycodes X11 has assigned to your keys.

$ python plot.py # requires matplotlib

--- EXAMPLE ---

This is my own output from "ascii_keyboard.py" after a couple of days
of collecting data:

(Using my own keyboard layout that's based on Colemak.)

  ┌─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┲━━━━━━━━━━┓
  │<?49>│  1  │  2  │  3  │  4  │  5  │  6  │  7  │  8  │  9  │  0  │<?20>│copyr┃BackSpace ┃
  │ 23  │ 993 │1073 │ 298 │ 274 │ 291 │ 156 │ 93  │ 70  │ 80  │ 394 │ 30  │ 15  ┃    5     ┃
  ┢━━━━━┷━━┱──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┺━━┳━━━━━━━┫
  ┃  Tab   ┃  q  │  w  │  f  │  p  │  g  │  j  │  l  │  u  │  y  │oslas│aring│ ae  ┃       ┃
  ┃  3598  ┃ 667 │1061 │3102 │4196 │3633 │ 796 │5573 │3575 │3198 │ 233 │1002 │ 104 ┃ Return┃
  ┣━━━━━━━━┻┱────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┺┓ 6823 ┃
  ┃BackSpace┃  a  │  r  │  s  │  t  │  d  │  h  │  n  │  e  │  i  │  o  │quote│quote┃      ┃
  ┃  16237  ┃6880 │7709 │8196 │9939 │4468 │2882 │9029 │13804│6781 │7002 │1145 │ 441 ┃      ┃
  ┣━━━━━━┳━━┹──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┬──┴──┲━━┷━━━━━┻━━━━━━┫
  ┃Shift_┃slash│  z  │  x  │  c  │  v  │  b  │  k  │  m  │comma│perio│minus┃    Shift_R    ┃
  ┃ 7199 ┃ 581 │1145 │2030 │3471 │2057 │2444 │2594 │3903 │1529 │2073 │ 604 ┃      840      ┃
  ┣━━━━━━┻┳━━━━┷━━┳━━┷━━━━┱┴─────┴─────┴─────┴─────┴─────┴─┲━━━┷━━━┳━┷━━━━━╋━━━━━━━┳━━━━━━━┫
  ┃Control┃Super_L┃ Alt_L ┃             space              ┃script_┃Super_R┃       ┃Control┃
  ┃ 5672  ┃ 1468  ┃ 1536  ┃             19088              ┃ 6335  ┃  206  ┃       ┃  513  ┃
  ┗━━━━━━━┻━━━━━━━┻━━━━━━━┹────────────────────────────────┺━━━━━━━┻━━━━━━━┻━━━━━━━┻━━━━━━━┛

As you can see, BackSpace is one of my most-used keys. Oof! I use it
/so much/ when programming.

So at least for me, it makes zero sense to have BackSpace in its
actual normal location.

