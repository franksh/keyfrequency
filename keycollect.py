
from time import time
from collections import Counter
from keylogger import KeyLogger
from pathlib import Path

# Log file. Will simply contain print() of a counter dictionary.

logfile = Path('data.coll')

# No timestamps or actual key sequences are stored. Only the 'key
# down' is counted. The data is only updated every 200 keystrokes.



if logfile.exists():
  # Laziest load ever.
  count = eval(logfile.open().read())
else:
  count = Counter()

st = time()

keylog = KeyLogger() # The actual keylogger.
n = sum(count.values())

print("Logging key usage...")

# Loop over key events.
for ev in keylog:
  if ev.seq or not ev.down:
    # Skip repetitions.
    continue
  
  count[ev.keycode] += 1
  n += 1
  
  if n % 200 == 0:
    print(f"{n} keys logged, 20 most common:")
    for c,k in count.most_common(30):
      print(f"{k:5d} <{c}>")
    with open('data.coll', 'w') as fil:
      print(count, file=fil)
