
import re
from pathlib import Path
from collections import Counter
R = lambda x,y: range(x, y+1)

### Settings.

logfile = Path('data.coll')

# My own key locations on pc105 Norwegian keyboard, here ordered by rows. Might be different for you.
keylocs = [49, R(10, 22),
           R(23,36),
           66, R(38,48), 51,
           50, 94, R(52,62),
           37, 133, 64, 65, 108, 134, 105]

### Script.

if not logfile.exists():
  raise RuntimeError("file data.coll not found")
counts = eval(logfile.open().read())

# ASCII drawing of keyboard.
asckbd = open('keyboard.txt').read()
keylocs = sum(([x] if isinstance(x, int) else list(x) for x in keylocs), [])

assert len(keylocs) == asckbd.count('.')

def next_line(s, pos):
  try:
    i = s.rindex('\n', 0, pos)
  except ValueError:
    i = -1
  try:
    j = s.index('\n', pos)
  except ValueError:
    j = len(s)
  return j + pos - i

def little_box(s, pos):
  ns = ne = next_line(s, pos)
  while s[ne].isspace():
    ne += 1
  return pos, ns, ne - ns

markers = [m.start() for m in re.finditer(r'\.', asckbd)]

def put_ascii_key(s, mrk, t1, t2):
  t1 = str(t1)
  t2 = str(t2)
  a,b,L = little_box(s, mrk)
  t1 = t1[:min(L,len(t1))]
  t2 = t2[:min(L,len(t2))]
  s = s[:a] + f'{t1:^{L}}' + s[a+L:]
  s = s[:b] + f'{t2:^{L}}' + s[b+L:]
  return s



from Xlib import X, protocol, XK
from Xlib.display import Display
from Xlib.ext import record
XKMap = dict([(getattr(XK, x),x[3:]) for x in dir(XK) if x.startswith('XK_')])
disp = Display()

for mrk,key in zip(markers, keylocs):
  kstr = disp.keycode_to_keysym(key,0)
  kstr = XKMap.get(kstr)
  kstr = kstr or f'<?{key}>'

  c = counts.get(key, 0)
  asckbd = put_ascii_key(asckbd, mrk, kstr, c)

print(asckbd)

