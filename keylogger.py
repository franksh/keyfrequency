from Xlib import X, protocol, XK
from Xlib.display import Display
from Xlib.ext import record
from time import time
from queue import SimpleQueue
from collections import namedtuple, Counter
from threading import Thread

KeyEvent = namedtuple('KeyEvent', 'keycode down seq time')

class KeyLogger(Thread):
  def __init__(self, queue=None):
    super().__init__()
    self.q = queue or SimpleQueue()
    self.ctxt = None
    self.display = None
    self.setDaemon(True)

  def __iter__(self):
    if not self.is_alive():
      self.start()
      assert self.q.get() == 'started'
    return self

  def __next__(self):
    if not self.is_alive():
      raise StopIteration()
    try:
      return self.q.get()
    except KeyboardInterrupt:
      raise StopIteration()

  def _event(self, reply):
    data = reply.data
    while len(data):
      event, data = protocol.rq.EventField(None).parse_binary_value(data, self.display.display, None, None)
      if event.type == X.KeyPress:
        down = True
      elif event.type == X.KeyRelease:
        down = False
      else:
        continue

      self.q.put(KeyEvent(event.detail, down, event.sequence_number, event.time))

  def stop(self):
    self.display.record_disable_context(self.ctx)

  def run(self):
    assert self.display is None
    self.q.put('started')
    self.display = Display()
    self.ctx = self.display.record_create_context(
      0,
      [record.AllClients],
      [{
        'core_requests': (0, 0),
        'core_replies': (0, 0),
        'ext_requests': (0, 0, 0, 0),
        'ext_replies': (0, 0, 0, 0),
        'delivered_events': (0, 0),
        'device_events': (X.KeyPress, X.KeyRelease),
        'errors': (0, 0),
        'client_started': False,
        'client_died': False,
      }]
    )
    self.display.record_enable_context(self.ctx, self._event)
    self.display.record_free_context(self.ctx)
    self.display = None
    self.ctxt = None
    self.q.put('stopped')
