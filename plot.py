
from pathlib import Path
from collections import Counter
logfile = Path('data.coll')
if not logfile.exists():
  raise RuntimeError("file data.coll not found")
count = eval(logfile.open().read())


from Xlib import X, protocol, XK
from Xlib.display import Display
from Xlib.ext import record
XKMap = dict([(getattr(XK, x),x[3:]) for x in dir(XK) if x.startswith('XK_')])
disp = Display()


import matplotlib.pyplot as plt

kix = []
for k,c in count.items():
  ks = disp.keycode_to_keysym(k,0)
  ks = XKMap.get(ks)
  ks = ks or f'<?{k}>'
  kix.append(ks)
  print(f'{ks:10s} {c}')

plt.barh(kix, count.values())

plt.show()


